### Manual de instalacion

# Aplicacion de gestión Clientes, Productos y Pedidos. Utilizando Graphql Client y React de lado del front y Graphql server y MongoDB en el Bancked

# Prerequisitos
> Tener instalado:  mongodb y nodejs 


# Pasos para instalar en un ambiente local

## Instrucciones

- Instalar mongodb 

- Modificar la variable de entorno path y agregamos el valor  de la ruta donde tenemos instalado mongodb

```
C:\Program Files\MongoDB\Server\4.0\bin
```

- Crear el directorio data/db y luego dentro de este directorio corremos el comando para levantar mongo

```
mongod
```

- Clonar el repositorio

```
git clone https://gitlab.com/javier.mesias.everis/pedidos.git
```

- Entrar al directorio de nombre cliente y ejecutamos:

```
npm install
```

- Entrar al directorio de nombre servidor y ejecutamos:

```
npm install
``` 

- Dentro del directorio servidor ejecutamos: 

```
npm start
``` 

Esto levantara el servidor graphql en el puerto 4000 [http://localhost:4000/graphql](http://localhost:4000/graphql). 


- Dentro del directorio cliente ejecutamos: 

```
npm start
``` 

Esto levantara el la aplicacion cliente react en el puerto 3000 [http://localhost:3000](http://localhost:3000). 


## Fin instalación local
