import gql from 'graphql-tag';

export const CLIENTES_QUERY	= gql`
query getClientes($limit: Int,$offset: Int ) {
  getClientes(limit: $limit, offset: $offset) {
    id
    nombre
    apellido
    empresa
  }
  totalClientes
}`;


export const CLIENTE_QUERY	= gql`
  query ConsultaCliente($id: ID!) {
	  getCliente(id: $id) {
		    id
		    nombre
		    apellido
		    empresa
		    edad
		    emails {
		      	email
		    },
		    tipo
	   }
	}
`;




export const PRODUCTO_QUERY	= gql`
  query getProducto($id: ID!) {
	  getProducto(id: $id) {
		     id
		    nombre
		    precio
		    stock
	   }
	}
`;

export const PRODUCTOS_QUERY	= gql`
query getProductos($limit: Int,$offset: Int,$stock: Boolean ) {
  getProductos(limit: $limit, offset: $offset, stock: $stock) {
  	id
    nombre
    precio
    stock
  }
  totalProductos
}`;



export const PEDIDOS_CLIENTE_QUERY	= gql`
query obtenerPedido($clienteId: String!) {
	  obtenerPedido(clienteId: $clienteId) {
		    id
		    total
		    fecha
		    estado
		    pedido {
			      id
			      cantidad
		    }
	  }
}`;


export const TOP_CLIENTES	= gql`
query topClientes {
	  topClientes {
		    total
		    cliente{
			      nombre
			      apellido
		    }
	  }
}`;