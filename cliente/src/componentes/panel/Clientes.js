import React , {Fragment} from 'react';
import { Query } from 'react-apollo';
import { TOP_CLIENTES } from '../../queries';
import '../../spinner.css';

const Clientes = () => {
		return (
               <Query query={TOP_CLIENTES}  
								pollInterval={10000000}
				>

				{({ loading, error, data, startPolling, stopPolling }) => { 

						if (loading) return (
								<div className="spinner">
									  <div className="double-bounce1"></div>
									  <div className="double-bounce2"></div>
								</div>
						)

						if (error) return `Error: ${error.message}`;


						return( 
							<p></p>

						);

				}}
                		
                
                </Query>
		);
}

export default Clientes;