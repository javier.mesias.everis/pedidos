import React, { Fragment } from 'react';
import { Query } from 'react-apollo';
import { PEDIDOS_CLIENTE_QUERY } from '../../queries';
import DetallePedido from './DetallePedido';
import '../../spinner.css';

const PedidoCliente = (props) => {
		const clienteId = props.match.params.id;
		console.log(clienteId)
		return (
				<Fragment>
	                <h1 className="text-center mb-5">Pedido</h1>
	                <div className="row">
	                		
	                		<Query query={PEDIDOS_CLIENTE_QUERY}  
								pollInterval={1000} 
							    variables={{clienteId}} 
					        >

					        	{({ loading, error, data, startPolling, stopPolling }) => { 

					        		if (loading) return (
											<div className="spinner">
												  <div className="double-bounce1"></div>
												  <div className="double-bounce2"></div>
											</div>
									)

					        		if (error) return `Error: ${error.message}`;
					        		
					        		return(
					        			 data.obtenerPedido.map( pedido => (
					        			 		<DetallePedido
					        			 		   key={pedido.id}
					        			 			pedido={pedido}
					        			 			clienteId={clienteId}
					        					/>

					        			 ) )
					        			
					        		)
					        			

					        	}}



					        </Query>
	                </div>
                </Fragment>
		);
}

export default PedidoCliente;