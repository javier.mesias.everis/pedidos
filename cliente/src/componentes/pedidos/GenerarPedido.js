import React from 'react';
import { Mutation } from 'react-apollo';
import { NUEVO_PEDIDO } from '../../mutations';
import { withRouter } from 'react-router-dom';

const  validarPedido = (props) => {
	return  props.total <= 0 || props.total === null || isNaN(props.total);

}

const GenerarPedido = (props) => {
	
	return (
			<Mutation mutation={NUEVO_PEDIDO}
							onCompleted={ () => {
                                 	props.history.push('/clientes')
                        	}}
			>
				{ nuevoPedido => ( 
					<button
						disabled={validarPedido(props)}
						type="button"
						className="btn btn-warning mt-4"
						onClick={ e => {
									// lo que hace aca es quitar nombre, precio, stock ya que PedidoProducto 
									// solo esta formado por id,cantidad  ejemplo : type PedidoProducto { id: ID cantidad: Int}
									const productosInput = props.productos.map(({nombre,precio,stock, ...objeto}) => objeto);
									const input = {	
										pedido: productosInput,
										total: props.total,
										cliente: props.idCliente
									}

									nuevoPedido({
											variables: {input}
									})

						}}

					>
					Generar Pedido
					</button>
				)}
			</Mutation>
		);
}

export default withRouter(GenerarPedido)