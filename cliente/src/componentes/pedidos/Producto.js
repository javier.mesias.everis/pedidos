import React , {Component , Fragment  } from 'react';

class Producto extends Component {

     state = {
     	
      }

     render(){
          const {producto} = this.props;

          return (
               <Fragment>
                    <tr>
                         <td>{producto.nombre}</td>
                         <td>$ {producto.precio}</td>
                         <td>{producto.stock}</td>
                         <td>
                              <input type="number" 
                                        min={1}
                                        className="form-control" 
                                        defaultValue={0}
                                        onChange={ e => {
                                                  if (e.target.value > producto.stock){
                                                       e.target.value = 0;
                                                  }
                                                  this.props.actualizarCantidad(e.target.value,this.props.index)
                                        }}
                              />
                         </td>
                         <td>
                              <button className="btn btn-danger font-weight-bold"
                                           onClick={ e => this.props.eliminarProducto(producto.id) }
                              >
                              Eliminar
                              </button>
                         </td>
                    </tr>
               </Fragment>
          );
     }
}

export default Producto;