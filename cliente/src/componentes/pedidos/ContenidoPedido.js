import React , {Component , Fragment  } from 'react';
import Select from 'react-select';
import Animated from 'react-select/lib/animated'
import Resumen from './Resumen';
import GenerarPedido from './GenerarPedido';
import Advertencia from '../alertas/Advertencia';

class ContenidoPedido extends Component {

     state = {
     		productos: [],
     		total: 0
      }

     seleccionarProductos = (productos,item) => {

          let productosAux = this.state.productos;

     	this.setState({
     			productos,
     	}, () => {
               if (typeof item !== 'undefined'){
                    if (item.action === 'remove-value'){
                         const {id} = item.removedValue;
                         this.eliminarProducto(id);
                    }
               }
          });

     }

     actualizarTotal = () => {

     		const productos = this.state.productos;
     		let nuevoTotal = 0;

     		if (productos.length === 0){
     			this.setState({ total: nuevoTotal  });
     			return;
     		}
               console.log(productos);
     		productos.map( producto => nuevoTotal += ( producto.precio * Number(producto.cantidad)    ));

     		this.setState({ total:nuevoTotal });

     }

     actualizarCantidad = (cantidad, index) => {
     		const productos = this.state.productos;
               let _cantidad = Number(cantidad);
               if (_cantidad > 0)
     		   productos[index].cantidad = _cantidad ;
               else
                    productos[index].cantidad = 'NaN' ;    
               
     		this.setState({ productos} , () => {  this.actualizarTotal()  } );
     }

     eliminarProducto = (productoId) => {
     		const productos = this.state.productos;
     		const productosRestantes = productos.filter(producto => producto.id !== productoId);
     		this.seleccionarProductos(productosRestantes);
     		this.setState({ productos : productosRestantes } , () => {  this.actualizarTotal()  } );


     }


     render(){

     		const { productos , id } = this.props;
               console.log('Total:' +this.state.total)
               const mensaje = (isNaN(this.state.total) ) ? <Advertencia mensaje="El total debe ser mayor a cero" /> : "";
     		return (
     			<Fragment>
     				<h3 className="text-center mb-5"> Seleccionar productos </h3>
                         {mensaje}
	     			<Select
	     				onChange={this.seleccionarProductos}
	     				options={productos}
	     				isMulti={true}
	     				components={Animated()}
	     				placeholder={'seleccionar un producto'}
	     				getOptionValue={ (options) => options.id }
	     				getOptionLabel={ (options) => options.nombre }
	     				value={this.state.productos}

	     			/>
	     			{ 

	     				this.state.productos.length > 0 ?  
	     				
		     				<Resumen 
		     				productos= { this.state.productos } 
		     				actualizarCantidad = { this.actualizarCantidad }
		     				eliminarProducto = { this.eliminarProducto }
		     				/> 
	     				 :  ''    

	     			}
	     			<p className="font-weight-bold float-right mt-3">
		     					Total:&nbsp; 
		     					<span className="font-weight-normal">
		     						$ { isNaN(this.state.total) ? 0 : this.state.total  }
		     					</span>
		     		</p>
                         <GenerarPedido 
                              productos={this.state.productos} 
                              total={this.state.total} 
                              idCliente={this.props.id}
                         />
     			</Fragment>
     	
     		);
     }
}

export default ContenidoPedido;