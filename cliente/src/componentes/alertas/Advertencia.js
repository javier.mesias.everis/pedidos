import React from 'react';

const Advertencia = ({ mensaje }) => {
		return (
                <p className="alert alert-warning py-3 text-center my-3" >
                		<b>{mensaje}</b>
                </p>
		);
}

export default Advertencia;