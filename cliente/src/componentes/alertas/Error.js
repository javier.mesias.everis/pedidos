import React from 'react';

const Error = ({ error }) => {
		return (
                <p className="alert alert-danger py-3 text-center my-3" >
                		<b>{error}</b>
                </p>
		);
}

export default Error;