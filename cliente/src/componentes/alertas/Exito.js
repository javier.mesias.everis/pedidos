import React from 'react';

const Exito = ({ mensaje }) => {
		return (
                <p className="alert alert-success py-3 text-center my-3" >
                		<b>{mensaje}</b>
                </p>
		);
}

export default Exito;