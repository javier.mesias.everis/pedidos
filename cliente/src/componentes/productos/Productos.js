import React , {Component, Fragment} from 'react';
import { Query , Mutation } from 'react-apollo';
import { PRODUCTOS_QUERY } from '../../queries';
import { ELIMINAR_PRODUCTO } from '../../mutations';
import { Link } from 'react-router-dom';
import Paginador from '../Paginador';
import Exito from '../alertas/Exito';
import SweetAlert from 'react-bootstrap-sweetalert';

const inicialState = {
			alerta:{
				mostrar: false,
				mensaje: ''
			},
			paginador : {
				offset:  0,
				actual: 1
			},
			mostrarSweetAlert : false,
			idEliminar: null
	}

class Productos extends Component {

	limite = 10;
	state = {
			...inicialState
	}

	idAEliminar = null;

	paginaAnterior =  () => {
		this.setState({
				paginador: {
						offset : this.state.paginador.offset - this.limite,
						actual: this.state.paginador.actual - 1
				}
		})
	}

	paginaSiguiente =  () => {
		this.setState({
				paginador: {
						offset : this.state.paginador.offset + this.limite,
						actual: this.state.paginador.actual + 1
				}
		})
	}

	onConfirm = () => {

		this.setState({mostrarSweetAlert : false })
	}

	onCancel = () => {
		this.setState({mostrarSweetAlert : false,idEliminar:null})
	}


	render() {

		const { alerta: { mostrar,mensaje } } = this.state;
		const alerta = (mostrar) ? <Exito mensaje={mensaje}  /> : '';

		return (
					<Query query={PRODUCTOS_QUERY}  
								pollInterval={100000000000000000} 
								fetchPolicy={'network-only'}
							    variables={{limit:this.limite,offset:this.state.paginador.offset}} 
					>
					{({ loading, error, data, startPolling, stopPolling }) => {

						if (loading) return "Cargando..";
						if (error) return `Error: ${error.message}`;
						//console.log(data.getProductos);

						return (
							<Fragment>
								{ (this.state.mostrarSweetAlert) ? 
									<Mutation 
																	mutation={ELIMINAR_PRODUCTO}
																	refetchQueries= {
																		() => ([{query: PRODUCTOS_QUERY, variables:{limit:this.limite,offset:this.state.paginador.offset}}])
																	}
																	onCompleted={ (data) => {
																			this.setState({
																					alerta:{
																						mostrar:true,
																						mensaje: data.eliminarProducto
																					}
																			}, () => {
																					setTimeout( () => {
																							this.setState({
																								...inicialState
																							})
																					}, 2000)
																			})
																	  }}	
										>
										{eliminarProducto => ( 
										<SweetAlert
												custom
												showCancel
												confirmBtnText="Yes"
												cancelBtnText="No"
												confirmBtnBsStyle="success"
												cancelBtnBsStyle="danger"
												style={{background:'#FFF'}}
												title="Eliminar producto!"
												onConfirm={  () => {  
																							let id = this.state.idEliminar;
																							eliminarProducto({
																											variables: {id}
																									})
																							this.setState({...this.state,mostrarSweetAlert : false, idEliminar:null})
																							
																			}}	
												onCancel={this.onCancel}
												>
												Estas seguro de eliminar este producto?
												</SweetAlert>
											)}
											</Mutation>
								 : ''  }
								<h2 className="text-center mt-4"> Litado Productos </h2>
								{alerta}
								<table className="table" >
										<thead>
											<tr className="table-primary">
													<th scope="col" >Nombre</th>
													<th scope="col" >Precio</th>
													<th scope="col" >Stock</th>
													<th scope="col" >Editar </th>
													<th scope="col" >Eliminar</th>
											</tr>
										</thead>
										<tbody>
											{
													data.getProductos.map( item => {
													const {id} = item;
													return (
															
															<tr key={id}>
																<td>{item.nombre}</td>
																<td>{item.precio}</td>
																<td>{item.stock}</td>
																<td>
																	<Link to={`/producto/editar/${item.id}`} className="btn btn-success d-block d-md-inline-block mr-2">
																		Editar
																	</Link>
																</td>
																<td>
																	
																		
																			<button 
																					type="button" 

																					className="btn btn-danger d-block d-md-inline-block mr-2"
																					
																					onClick= {  () => {  
																							this.setState({mostrarSweetAlert : true , idEliminar:id})
																					}}	
																			>
																				 Eliminar
																			</button>
																						
																		
																	
																</td>
															</tr>		

													)})

											}
										</tbody>	
									</table>
								<h5 className="text-right mt-4">Total productos <span class="badge badge-success">{data.totalProductos}</span></h5>
								<Paginador  
										actual={this.state.paginador.actual}  
										totalregistro = {data.totalProductos}
										limite={this.limite}
										paginas={ Math.ceil( Number(data.totalProductos) / this.limite ) }
										paginaAnterior={this.paginaAnterior}
										paginaSiguiente={this.paginaSiguiente}

								/>
							</Fragment>
							
						);

					}}
					</Query>
		)
	}

}


export default Productos;