import React, { Component } from 'react';
import { ACTUALIZA_CLIENTE } from '../../mutations';
import { Mutation } from 'react-apollo';
import { withRouter } from 'react-router-dom';

class FormularioEditarCliente extends Component {

    state =  {
        cliente: this.props.cliente,
        emails: this.props.cliente.emails,
        error: false,
    }

    nuevoCampo = () => {
        this.setState({
            emails: this.state.emails.concat([{email:''}])
        })
    }

    leerCampo = (key) => (event) => {
        const nuevoMail = this.state.emails.map((email, index) => {
                if (key !== index) return email;
                return { 
                            ...email, 
                            email: event.target.value 
                };
        });
        this.setState({ emails: nuevoMail });
    }

    quitarCampo = (key) => () => {
        this.setState({
            emails: this.state.emails.filter((s, index) => key !== index)
        });
    }



    render() { 
            const { nombre, apellido, empresa, edad, tipo } = this.state.cliente;
            const {emails} = this.state;

            const { error } = this.state;
            let respuesta = (error) ? 
                <p className="alert alert-danger p-3 text-center">
                los campos nombre, apellido, empresa, tipo son obligatorios
                </p> : '';
           
            return (
                    <Mutation 
                        mutation={ACTUALIZA_CLIENTE}
                        onCompleted={ () =>this.props.refetch().then( () => {
                                 this.props.history.push('/clientes')
                        })} 
                    >
                    {actualizarCliente => ( 
                       <form className="col-md-8 m-3"
                            onSubmit={ (e) => {
                                e.preventDefault();
                                const { id, nombre, apellido, empresa, edad, tipo } = this.state.cliente;
                                const {emails} = this.state;
                                const input = {
                                            id,
                                            nombre,
                                            apellido,
                                            empresa,
                                            edad : Number(edad),
                                            emails,
                                            tipo
                                    };

                                    if (
                                        nombre === '' ||
                                        apellido  === '' ||
                                        empresa === '' ||
                                        tipo === ''
                                        ) {
                                        console.log(34234);
                                        this.setState({ error: true });
                                        return;
                                    }

                                    this.setState({ error: false });

                                   actualizarCliente({
                                            variables: {input}
                                    })

                        }}>
                                 {respuesta}
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label>Nombre</label>
                                        <input
                                            type="text" 
                                            className="form-control" 
                                            defaultValue={nombre}
                                            required={true}
                                            onChange={e => {
                                                this.setState({
                                                    cliente:{
                                                            ...this.state.cliente, 
                                                            nombre: e.target.value 
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Apellido</label>
                                        <input 
                                            type="text" 
                                            className="form-control" 
                                            defaultValue={apellido}
                                            required={true}
                                            onChange={e => {
                                                this.setState({
                                                    cliente:{
                                                            ...this.state.cliente, 
                                                            apellido: e.target.value 
                                                    }
                                                })
                                            }}
                                         />
                                    </div>
                                </div>
                              
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label>Empresa</label>
                                        <input
                                            type="text" 
                                            className="form-control" 
                                            defaultValue={empresa}
                                            required={true}
                                            onChange={e => {
                                                this.setState({
                                                    cliente:{
                                                            ...this.state.cliente, 
                                                            empresa: e.target.value 
                                                    }
                                                })
                                            }}
                                        />
                                    </div>

                                    {emails.map((input, index) => (
                                        <div key={index} className="form-group col-md-12">
                                            <label>Email {index + 1} : </label>
                                            <div className="input-group">
                                            
                                                <input 
                                                    type="email"
                                                    placeholder={`Email`}
                                                    className="form-control" 
                                                    onChange={this.leerCampo(index)}
                                                    defaultValue={input.email}
                                                />
                                                <div className="input-group-append">
                                                    <button 
                                                        className="btn btn-danger" 
                                                        type="button" 
                                                        onClick={this.quitarCampo(index)}> 
                                                        Eliminar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                    <div className="form-group d-flex justify-content-center col-md-12">
                                        <button 
                                            onClick={this.nuevoCampo}
                                            type="button" 
                                            className="btn btn-warning"
                                        >Agregar Email</button>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label>Edad</label>
                                        <input
                                            type="text" 
                                            className="form-control" 
                                            defaultValue={edad}
                                            onChange={e => {
                                                this.setState({
                                                    cliente:{
                                                            ...this.state.cliente, 
                                                            edad: e.target.value 
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label>Tipo Cliente</label>  
                                        <select 
                                            className="form-control"
                                            value={tipo}
                                            required={true}
                                            onChange={e => {
                                                this.setState({
                                                    cliente:{
                                                            ...this.state.cliente, 
                                                            tipo: e.target.value 
                                                    }
                                                })
                                            }}
                                        >
                                            <option value="">Elegir...</option>
                                            <option value="PREMIUM">PREMIUM</option>
                                            <option value="BASICO">BÁSICO</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-success float-right">Guardar Cambios</button>
                            </form>
                        )}
                        </Mutation>
            )
    }
}
 

export default withRouter(FormularioEditarCliente);