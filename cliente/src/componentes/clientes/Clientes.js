import React , {Component, Fragment} from 'react';
import { Query , Mutation } from 'react-apollo';
import { CLIENTES_QUERY } from '../../queries';
import { ELIMINAR_CLIENTE } from '../../mutations';
import { Link } from 'react-router-dom';
import Paginador from '../Paginador';
import Exito from '../alertas/Exito';
import SweetAlert from 'react-bootstrap-sweetalert';
import '../../spinner.css';

const inicialState = {
			alerta:{
				mostrar: false,
				mensaje: ''
			},
			paginador : {
				offset:  0,
				actual: 1
			},
			mostrarSweetAlert : false,
			idEliminar: null
	}

class Clientes extends Component {

	limite = 10;
	state = {
		...inicialState
	}

	paginaAnterior =  () => {
		this.setState({
				paginador: {
						offset : this.state.paginador.offset - this.limite,
						actual: this.state.paginador.actual - 1
				}
		})
	}

	paginaSiguiente =  () => {
		this.setState({
				paginador: {
						offset : this.state.paginador.offset + this.limite,
						actual: this.state.paginador.actual + 1
				}
		})
	}

	onConfirm = () => {

		this.setState({mostrarSweetAlert : false })
	}

	onCancel = () => {
		this.setState({mostrarSweetAlert : false,idEliminar:null})
	}

	render() {
		const { alerta: { mostrar,mensaje } } = this.state;
		const alerta = (mostrar) ? <Exito mensaje={mensaje}  /> : '';
		return (
					<Query query={CLIENTES_QUERY}  
								pollInterval={100000000000000000} 
								fetchPolicy={'network-only'}
							    variables={{limit:this.limite,offset:this.state.paginador.offset}} 
					>
					{({ loading, error, data, startPolling, stopPolling }) => {

						if (loading) return (
								<div className="spinner">
									  <div className="double-bounce1"></div>
									  <div className="double-bounce2"></div>
								</div>
						)

						if (error) return `Error: ${error.message}`;
						
						
						return (
							<Fragment>
													{ (this.state.mostrarSweetAlert) ? 
									<Mutation 
																	mutation={ELIMINAR_CLIENTE}
																	refetchQueries= {
																		() => ([{query: CLIENTES_QUERY, variables:{limit:this.limite,offset:this.state.paginador.offset}}])
																	}
																	onCompleted={ (data) => {
																			this.setState({
																					alerta:{
																						mostrar:true,
																						mensaje: data.eliminarCliente
																					}
																			}, () => {
																					setTimeout( () => {
																							this.setState({
																								...inicialState
																							})
																					}, 2000)
																			})
																	  }}	
										>
										{eliminarCliente => ( 
										<SweetAlert
												custom
												showCancel
												confirmBtnText="Yes"
												cancelBtnText="No"
												confirmBtnBsStyle="success"
												cancelBtnBsStyle="danger"
												style={{background:'#FFF'}}
												title="Eliminar cliente!"
												onConfirm={  () => {  
																							let id = this.state.idEliminar;
																							eliminarCliente({
																											variables: {id}
																									})
																							this.setState({...this.state,mostrarSweetAlert : false, idEliminar:null})
																							
																			}}	
												onCancel={this.onCancel}
												>
												Estas seguro de eliminar este cliente?
												</SweetAlert>
											)}
											</Mutation>
								 : ''  }
								<h2 className="text-center mt-4"> Litado Clientes </h2>
								{alerta}
								<ul className="list-group mt-4">
									{
										data.getClientes.map( item => {
										const {id} = item;
										return (
												<li key={item.id} className="list-group-item" >
												<div className="row justify-content-between align-items-start">
													<div className="col-md-8 d-flex  justify-content-between align-items-center">
														{item.nombre} { item.apellido }
													</div>
													<div className="col-md-6 d-flex justify-content-between align-items-center">
														 {item.empresa}
													</div>
													<div className="col-md-6 d-flex justify-content-end">
															<Link to={`/pedidos/nuevo/${id}`} className="btn btn-warning d-block d-md-inline-block mr-2">
																 Nuevo Pedido
															</Link>
															<Link to={`/pedidos/cliente/${id}`} className="btn btn-primary d-block d-md-inline-block mr-2">
																 Ver Pedido
															</Link>
															<Link to={`/cliente/editar/${item.id}`} className="btn btn-success d-block d-md-inline-block mr-2">
																Editar cliente
															</Link>
																	<button 
																			type="button" 
																			className="btn btn-danger d-block d-md-inline-block mr-2"
																			onClick= {  () => { 
																					this.setState({mostrarSweetAlert : true , idEliminar:id})
																			}}	
																	>
																		Eliminar
																	</button>
													</div>
												</div>
											</li>

										)})

									}
								</ul>
								<h5 className="text-right mt-4">Total Clientes <span className="badge badge-success">{data.totalClientes}</span></h5>
								<Paginador  
										actual={this.state.paginador.actual}  
										totalClientes = {data.totalClientes}
										limite={this.limite}
										paginas={ Math.ceil( Number(data.totalClientes) / this.limite ) }
										paginaAnterior={this.paginaAnterior}
										paginaSiguiente={this.paginaSiguiente}

								/>
							</Fragment>
							
						);

					}}
					</Query>
		)
	}

}


export default Clientes;