import React , { Component } from 'react';

class Paginador extends Component {

		state = {

		 }
		
		render(){
				const { actual,paginas } = this.props;
				const btnAnterior = ( actual > 1 ) ? <button type="button"  onClick={this.props.paginaAnterior} className="btn btn-success mr-2">
																				&laquo; Anterior
																			</button> : ''; 

				const btnSiguiente = ( actual  !== paginas ) ? <button type="button" onClick={this.props.paginaSiguiente} className="btn btn-success mr-2">
																				Siguiente &raquo;
																			</button> : ''; 

				return (
						
						<div className="mt-5 d-flex justify-content-center mb-md-2">
									{btnAnterior}
								    {btnSiguiente}
						</div>

				);
		}

}

export default Paginador;