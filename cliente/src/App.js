import React, { Component , Fragment } from 'react';
import { ApolloProvider } from 'react-apollo';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './componentes/layout/Header';
import Clientes from './componentes/clientes/Clientes';
import NuevoCliente from './componentes/clientes/NuevoCliente';
import EditarCliente from './componentes/clientes/EditarCliente';


import Productos from './componentes/productos/Productos';
import NuevoProducto from './componentes/productos/NuevoProducto';
import EditarProducto from './componentes/productos/EditarProducto';

import NuevoPedido from './componentes/pedidos/NuevoPedido';
import PedidoCliente from './componentes/pedidos/PedidoCliente';
import Panel from './componentes/panel/Panel';

const  client = new ApolloClient({

  uri: "http://localhost:4000/graphql",
  cache: new InMemoryCache({
      addTypename: false
  }),
  onError: ({networkError, graphQLErrors}) => {
      console.log('graphQLErrors', graphQLErrors);
      console.log('networkError', networkError);
  }

});


class App extends Component {
  render() {
    return (
        <ApolloProvider client={client}>
          <Router>
            <Fragment>
                <Header/>
                <div className="container">
                  <Switch>
                    <Route exact path="/" component={Clientes} />
                    <Route exact path="/clientes" component={Clientes} />
                    <Route exact path="/cliente/nuevo" component={NuevoCliente} />
                    <Route exact path="/cliente/editar/:id" component={EditarCliente} />
                    <Route exact path="/producto/nuevo" component={NuevoProducto} />
                    <Route exact path="/productos" component={Productos} />
                    <Route exact path="/producto/editar/:id" component={EditarProducto} />
                    <Route exact path="/pedidos/nuevo/:id" component={NuevoPedido} />
                    <Route exact path="/pedidos/cliente/:id" component={PedidoCliente} />
                    <Route exact path="/panel" component={Panel} />
                  </Switch>
                </div>
            </Fragment>
           </Router>
        </ApolloProvider>
    );
  }
}

export default App;
