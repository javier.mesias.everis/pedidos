{
  uno: getCliente(id: "5c768ff5c2a7d836c44cc70e"){
    ... clientes
    emails{
      email
    }
  }
  
  dos: getCliente(id: "5c769308222b0934b486ce04"){
    ... clientes
  }
  
}


fragment clientes on Cliente{
  nombre
  apellido
  edad
  pedidos{
    producto
  }
}