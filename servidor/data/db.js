import mongoose from 'mongoose';


mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/clientes', { useNewUrlParser: true });

mongoose.set('setFindAndModify', false);
//definir el schema de cliente

const clientesSchema = new mongoose.Schema({
	// se define como va a ser la base de datos
	nombre: String,
	apellido: String,
	empresa: String,
	emails: Array,
	edad: Number,
	tipo: String,
	pedidos: Array 
});

//definir el schema de productos

const productosSchema = new mongoose.Schema({
	// se define como va a ser la base de datos
	nombre: String,
	precio: Number,
	stock: Number
});

//definir el schema de pedidos

const pedidosSchema = new mongoose.Schema({
	// se define como va a ser la base de datos
	pedido: Array,
	total: Number,
	fecha: Date,
	cliente: mongoose.Types.ObjectId,
	estado: String
});



const Clientes = mongoose.model('clientes',clientesSchema);
const Productos = mongoose.model('productos',productosSchema);
const Pedidos = mongoose.model('pedidos',pedidosSchema);

export { Clientes, Productos, Pedidos  };